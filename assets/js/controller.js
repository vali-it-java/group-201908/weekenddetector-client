
// Controller functions here...

function loadWeekendStatus() {
    let currentDateTime = getCurrentDateTimeString();
    fetchIsItWeekend(currentDateTime).then(
        function(result) {
            let weekendMessageDiv = document.getElementById("weekendMessage");

            if (result.status === "false") {
                weekendMessageDiv.style.color = "green";
                weekendMessageDiv.innerText = result.message;
            } else {
                weekendMessageDiv.style.color = "red";
                weekendMessageDiv.innerText = result.message + "!!!!";
            }

            document.getElementById("visitCountBox").innerText = result.visitCount;
        }
    );
}
