
function fetchIsItWeekend(currentDateTime) {
    return fetch(
        `${API_URL}/isItWeekend?datetime=${currentDateTime}`,
        {
            method: 'GET'
        }
    )
    .then(something => something.json());
}
