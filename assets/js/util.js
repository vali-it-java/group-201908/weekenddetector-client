
function getCurrentDateTimeString() {
    let currentDate = new Date();
    let currentYear = currentDate.getFullYear();
    let currentMonth = (currentDate.getMonth() + 1) < 10 ? 
        "0" + (currentDate.getMonth() + 1) : (currentDate.getMonth() + 1);
    let currentDay = currentDate.getDate() < 10 ? 
        "0" + currentDate.getDate() : currentDate.getDate();
    let currentHour = currentDate.getHours() < 10 ? 
        "0" + currentDate.getHours() : currentDate.getHours();
    let currentMinute = currentDate.getMinutes() < 10 ? 
        "0" + currentDate.getMinutes() : currentDate.getMinutes();
    let currentDateTime = `${currentYear}-${currentMonth}-${currentDay} ${currentHour}:${currentMinute}`;
    return currentDateTime;
}
